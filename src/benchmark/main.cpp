/*
 * main.cpp
 *
 *  Created on: Jan 3, 2012
 *      Author: kkb110
 */

#include <iostream>
#include <complex>
#include <vector>
#include <cstdlib>
#include <ctime>

#include <boost/assert.hpp>

#include <adolc/adouble.h>
#include <adolc/adolc.h>
#include <adolc/drivers/drivers.h>
#include <adolc/taping.h>
#define NDEBUG 1
#include <cppad/cppad.hpp>

#include "../kklautodiff.hpp"
//#include "ad.h"
#include "kissfft.h"
#include "badiff.h"
#define ADEPT_STACK_THREAD_UNSAFE
#include "adept.h"
#include <sys/time.h>

using namespace std;

#define DATA_SIZE 8192*8
#define LOOP_COUNT 7
#define CHECKSUM_INDEX (DATA_SIZE / 4 + 3)

double get_val(double x){
    return x;
}
template<typename T>
double get_val(fadbad::B<T> x){
    return x.x();
}
template<typename T>
double get_val(kklautodiff::RealNumber<T> x){
    return x.value;
}

static double now_ms(void){
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec * 1000. + tv.tv_usec / 1000.;
}

template<typename T>
T fftsum(T data[]){
    complex<T> input[DATA_SIZE], output[DATA_SIZE];
    for (int i = 0; i < DATA_SIZE; i++){
        input[i] = data[i];
    }
    kissfft<T> fft(DATA_SIZE);
    fft.transform(input, output);
    T total = 0.0;
    for (int i = 0; i < DATA_SIZE; i++){
        total += norm(output[i]);
    }
    return total;
}

template<typename T>
void benchmark(string name, vector<double> initial_input, T func){
    BOOST_ASSERT(initial_input.size() == DATA_SIZE);
    double start = now_ms();
    double checksum = 0;
    for (int i = 0; i < LOOP_COUNT; i++){
        size_t checksum_index = LOOP_COUNT % DATA_SIZE;
        checksum += func(initial_input, checksum_index);
    }
    double elapsed = now_ms() - start;
    cout << name << "\telapsed : " << elapsed << "\tchecksum : " << checksum
            << endl;
}

int main(){
    //data initialization
    srand(time(0));
    vector<double> original(DATA_SIZE);
    for (int i = 0; i < DATA_SIZE; i++){
        original[i] = (double) rand() / RAND_MAX / DATA_SIZE;
    }

    cout
            << "checksum compares the gradient computed with different libraries. Please ignore the \"double\"'s checksum."
            << endl;

    //double
    benchmark("double\t\t", original, [](vector<double> input, size_t checksum_index){return fftsum(&input[0]);});

    //kklautodiff
    benchmark("kklautodiff\t", original, [](vector<double> input, size_t checksum_index){
                static kklautodiff::RealNumber<double> x[DATA_SIZE];
                static kklautodiff::Tape<double> tape(DATA_SIZE * 10000);
                for (int i = 0; i < DATA_SIZE; i++){
                    x[i] = input[i];
                    x[i].attach_to_tape(tape);
                }
                kklautodiff::RealNumber<double> y = fftsum(x);
                y.diff();
                tape.end_pointer = tape.memory_block;
                return x[checksum_index].get_diff();
                ;});

    //adept
    benchmark("adept\t\t", original, [](vector<double> input, size_t checksum_index){
                static adept::Stack stack;
                stack.start(100000);
                static adept::adouble x[DATA_SIZE];
                for (int i = 0; i < DATA_SIZE; i++){
                    x[i].set_value(input[i]);
                }
                adept::adouble y = fftsum(x);
                y.set_gradient(1.0);
                stack.reverse();
                return x[checksum_index].get_gradient();
                ;});

    //cppadd
    CppAD::thread_alloc::hold_memory(true);
    benchmark("cppad\t\t", original, [](vector<double> input, size_t checksum_index){
                static size_t run_count = 0;
                static vector<CppAD::AD<double> > x(DATA_SIZE);
                static CppAD::ADFun<double> f;
                if (run_count == 0){
                    for (int i = 0; i < DATA_SIZE; i++){
                        x[i] = input[i];
                    }
                    CppAD::Independent(x);
                    CppAD::AD<double> sum = fftsum(&x[0]);
                    f = CppAD::ADFun<double>(x, vector<CppAD::AD<double>>(1,sum));
                }
//                ++run_count;
                return f.Jacobian(input)[checksum_index];
            });

    //cppadd(pre-taped)
    CppAD::thread_alloc::hold_memory(true);
    benchmark("cppad(pre-taped)", original, [](vector<double> input, size_t checksum_index){
                static size_t run_count = 0;
                static vector<CppAD::AD<double> > x(DATA_SIZE);
                static CppAD::ADFun<double> f;
                if (run_count == 0){
                    for (int i = 0; i < DATA_SIZE; i++){
                        x[i] = input[i];
                    }
                    CppAD::Independent(x);
                    CppAD::AD<double> sum = fftsum(&x[0]);
                    f = CppAD::ADFun<double>(x, vector<CppAD::AD<double>>(1,sum));
                }
                ++run_count;
                return f.Jacobian(input)[checksum_index];
            });

    // adol-c
    benchmark("adol-c\t\t", original, [](vector<double> input, size_t checksum_index){
                static size_t run_count = 0;
                static adouble x[DATA_SIZE];
                if (run_count == 0)
                {
                    trace_on(1);
                    for (int i = 0; i < DATA_SIZE; i++){
                        x[i] <<= input[i];
                    }
                    adouble y = fftsum(x);
                    double ret;
                    y >>= ret;
                    trace_off();
                }
                static double output_gradient[DATA_SIZE];
                gradient(1, DATA_SIZE, &input[0], output_gradient);
//                ++run_count;
                return output_gradient[checksum_index];
            });

    // adol-c(pre-taped)
    benchmark("adol-c(pre-taped)", original, [](vector<double> input, size_t checksum_index){
                static size_t run_count = 0;
                static adouble x[DATA_SIZE];
                if (run_count == 0)
                {
                    trace_on(1);
                    for (int i = 0; i < DATA_SIZE; i++){
                        x[i] <<= input[i];
                    }
                    adouble y = fftsum(x);
                    double ret;
                    y >>= ret;
                    trace_off();
                }
                static double output_gradient[DATA_SIZE];
                gradient(1, DATA_SIZE, &input[0], output_gradient);
                ++run_count;
                return output_gradient[checksum_index];
            });

    // fadbad
    benchmark("fadbad\t\t", original, [](vector<double> input, size_t checksum_index){
                static fadbad::B<double> x[DATA_SIZE];
                for (int i = 0; i < DATA_SIZE; i++){
                    x[i] = input[i];
                }
                fadbad::B<double> y = fftsum(x);
                y.diff(0,1);
                return x[checksum_index].d(0);
            });

    return 0;
}

