#ifndef KKLAUTODIFF_HPP
#define KKLAUTODIFF_HPP

#include <iostream>
#include <cmath>
#include <numeric>
#include <limits>
#include <boost/assert.hpp>

namespace kklautodiff {
enum ADOpcode{
    AD_NONE = 0, // none
    AD_ADD, // addition
    AD_SUB, // subtraction
    AD_DIV, // division
    AD_POS, // unary + operator
    AD_NEG, // unary - operator
    AD_POW, // power; pow(x,y)
    AD_SQRT, // square root
    AD_LOG, // natural logarithm
    AD_EXP, // exponential
    AD_SIN, // sine
    AD_COS, // cosine
    AD_TAN, // tangent
    AD_ASIN, // arcsine
    AD_ACOS, // arccos
    AD_ATAN, // arctan
    AD_FMOD,

    //    AD_ADD1, //addition with only op1
    //    AD_SUB1, //subtraction with only op1
    //    AD_MUL1, //multiplication with only op1
    //    AD_DIV2, // division with only op2   (i.e.  op1/op2)
    AD_POW1, //power with only op1 (i.e. pow(op1,op2))
    //    AD_POW2, //power with only op2 (i.e. pow(op1,op2))

    AD_MUL_CONSTANT = 0x8000,
    AD_MUL1 = 0x8002,
    AD_MUL2 = 0x8001,
    AD_MUL = 0x8003,
};

template<typename T>
class History{
public:
    ADOpcode operation_type;
    T value;
    T diff; //TODO I don't like saving diff here, it's not necessary until we do the actual differentiation.
    union {
        History<T>* op1; //TODO doesn't have to be a 8-byte pointer, 4-byte index or pointer should be sufficient.
        T op1_T;
    };
    union {
        History<T>* op2;
        T op2_T;
    };
    History(
            ADOpcode operation_type,
            T value,
            T diff,
            History<T>* op1,
            History<T>* op2) :
        operation_type(operation_type), value(value), diff(diff), op1(op1),
                op2(op2){
    }
    History(
            ADOpcode operation_type,
            T value,
            T diff,
            History<T>* op1,
            double op2_double) :
        operation_type(operation_type), value(value), diff(diff), op1(op1),
                op2_T(op2_double){
    }
};

template<typename T>
class Tape{
public:
    History<T>* memory_block;
    History<T>* end_pointer;
    History<T>* max_pointer;

    Tape()= delete;
    Tape(size_t initial_block_size){
        memory_block = static_cast<History<T>*> (operator new(
                sizeof(History<T> ) * initial_block_size)); // TODO can I use new without calling the constructor?
        BOOST_ASSERT_MSG(memory_block != nullptr, "malloc failed");
        end_pointer = memory_block;
        max_pointer = memory_block + initial_block_size;
    }

    ~Tape(){
        free(memory_block);
        memory_block = nullptr;
    }
};

template<typename T>
class RealNumber{
public:
    T value;
    Tape<T>* attached_tape;
    History<T>* attached_location_on_tape;

    explicit operator T() const{
        return value;
    }

    explicit operator int() const{
        return value;
    }

    explicit operator short unsigned int() const{
        return value;
    }

    explicit operator unsigned int() const{
        return value;
    }

    explicit operator double() const{
        return value;
    }

    explicit operator bool() const{
        return value;
    }

    RealNumber() :
        attached_tape(nullptr), attached_location_on_tape(nullptr){
    }

    RealNumber(const T& constant) :
        RealNumber(){
        value = constant; //TODO it must be possible to do mem-initializer and constructor delegation together. As of GCC4.7.0, it's not possible
    }

    RealNumber(
            T value,
            Tape<T>* attached_tape,
            History<T>* attached_location_on_tape) :
        value(value), attached_tape(attached_tape),
                attached_location_on_tape(attached_location_on_tape){
    }

    RealNumber(
            const ADOpcode& opcode,
            const T& computed_value,
            const RealNumber& op1){
        value = computed_value;
        attached_tape = op1.attached_tape;
        if (op1.attached_tape != nullptr){ //TODO without branch?
            BOOST_ASSERT_MSG(
                    op1.attached_tape->end_pointer
                    != op1.attached_tape->max_pointer,
                    "max tape size reached");
            new (op1.attached_tape->end_pointer) History<T> (opcode,
                    computed_value, 0, op1.attached_location_on_tape, nullptr); //TODO don't need to write nullptr
            attached_location_on_tape = op1.attached_tape->end_pointer++;
        }else{
            attached_location_on_tape = nullptr;
        }
    }

    RealNumber(
            const ADOpcode& opcode,
            const T& computed_value,
            const RealNumber& op1,
            const RealNumber& op2){
        //        this->value = computed_value;
        //        Tape<T>
        //                * common_attached_tape =
        //                        reinterpret_cast<Tape<T>*> (reinterpret_cast<uintptr_t> (op1.attached_tape)
        //                                & reinterpret_cast<uintptr_t> (op2.attached_tape));
        //        this->attached_tape = common_attached_tape;
        //        if (common_attached_tape != nullptr){ //TODO without branch?
        //            new (common_attached_tape->end_pointer) History<T> (opcode,
        //                    computed_value, 0, op1.attached_location_on_tape,
        //                    op2.attached_location_on_tape);
        //            this->attached_location_on_tape
        //                    = common_attached_tape->end_pointer++;
        //        }else{
        //            this->attached_location_on_tape = nullptr;
        //        }

        this->value = computed_value;
        if (op1.attached_tape != nullptr || op2.attached_tape != nullptr){ //TODO without branch?
            Tape<T> * common_attached_tape =
                    op1.attached_tape != nullptr
                                                 ? op1.attached_tape
                                                 : op2.attached_tape;
            BOOST_ASSERT_MSG(
                    common_attached_tape->end_pointer
                    != common_attached_tape->max_pointer,
                    "max tape size reached");
            new (common_attached_tape->end_pointer) History<T> (opcode,
                    computed_value, 0, op1.attached_location_on_tape,
                    op2.attached_location_on_tape);
            this->attached_tape = common_attached_tape;
            this->attached_location_on_tape
                    = common_attached_tape->end_pointer++;
        }else{
            this->attached_tape = nullptr;
            this->attached_location_on_tape = nullptr;
        }
    }

    //    RealNumber operator+(const RealNumber &other) const{
    //        return RealNumber(AD_ADD, value + other.value, *this, other);
    //    }

    friend RealNumber operator+(const RealNumber &lhs, const RealNumber &rhs){
        return RealNumber(AD_ADD, lhs.value + rhs.value, lhs, rhs);
    }

    friend RealNumber operator-(const RealNumber &lhs, const RealNumber &rhs){
        return RealNumber(AD_SUB, lhs.value - rhs.value, lhs, rhs);
    }

    friend RealNumber<T> operator*(
            const RealNumber<T>& lhs,
            const RealNumber<T>& rhs){ //TODO reference is better
        RealNumber result;
        result.value = lhs.value * rhs.value;
        if (lhs.attached_tape != nullptr || rhs.attached_tape != nullptr){ //TODO without branch?
            Tape<T> * common_attached_tape =
                    lhs.attached_tape != nullptr
                                                 ? lhs.attached_tape
                                                 : rhs.attached_tape;
            BOOST_ASSERT_MSG(
                    common_attached_tape->end_pointer
                    != common_attached_tape->max_pointer,
                    "max tape size reached");
            new (common_attached_tape->end_pointer) History<T> (
                    static_cast<ADOpcode> (AD_MUL_CONSTANT
                            | ((lhs.attached_tape != nullptr) << 1)
                            | (rhs.attached_tape != nullptr)),
                    result.value,
                    0,
                    (lhs.attached_tape != nullptr)
                                                   ? lhs.attached_location_on_tape
                                                   : ((History<T>*) *((uintptr_t*) &lhs.value)),
                    (rhs.attached_tape != nullptr)
                                                   ? rhs.attached_location_on_tape
                                                   : ((History<T>*) *((uintptr_t*) &rhs.value)));
            result.attached_tape = common_attached_tape;
            result.attached_location_on_tape
                    = common_attached_tape->end_pointer++;
            return result;
        }else{
            result.attached_tape = nullptr;
            result.attached_location_on_tape = nullptr;
            return result;
        }
        return result;
    }

    //    friend RealNumber operator/(T lhs, RealNumber rhs){
    //        RealNumber lhs_attached = lhs;
    //        if (rhs.attached_tape != nullptr){ //TODO this is a dirty hack
    //            lhs_attached.attach_to_tape(lhs.attached_tape);
    //            rhs.attach_to_tape(lhs.attached_tape);
    //        }
    //        return RealNumber(AD_DIV, lhs.value / rhs.value, lhs, rhs);
    //    }

    friend RealNumber operator/(RealNumber lhs, RealNumber rhs){
        if (lhs.attached_tape != nullptr && rhs.attached_tape == nullptr){ //TODO this is a dirty hack
            rhs.attach_to_tape(*lhs.attached_tape);
        }
        if (rhs.attached_tape != nullptr && lhs.attached_tape == nullptr){
            lhs.attach_to_tape(*rhs.attached_tape);
        }
        return RealNumber(AD_DIV, lhs.value / rhs.value, lhs, rhs);
    }

    RealNumber& operator+=(const RealNumber &other){
        return (*this) = (*this) + other;
    }

    RealNumber& operator-=(const RealNumber &other){
        return (*this) = (*this) - other;
    }

    RealNumber& operator*=(const RealNumber &other){
        return (*this) = (*this) * other;
    }

    RealNumber& operator/=(const RealNumber &other){
        return (*this) = (*this) / other;
    }

    RealNumber operator+() const{
        return *this;
    }

    RealNumber operator-() const{
        return RealNumber(AD_NEG, -value, *this);
    }

    friend RealNumber exp(const RealNumber& hs){
        return RealNumber(AD_EXP, exp(hs.value), hs);
    }

    friend RealNumber log(const RealNumber& hs){
        return RealNumber(AD_LOG, log(hs.value), hs);
    }

    friend RealNumber pow(const RealNumber& lhs, const RealNumber& rhs){
        RealNumber result = RealNumber(AD_POW, pow(lhs.value, rhs.value), lhs, rhs);
        return result;
    }

    friend RealNumber pow(const RealNumber& lhs, const T& rhs){
        RealNumber result = RealNumber(AD_POW1, pow(lhs.value, rhs), lhs);
        result.attached_location_on_tape->op2_T = rhs;
        return result;
    }

    friend RealNumber sqrt(const RealNumber& hs){
        return RealNumber(AD_SQRT, sqrt(hs.value), hs);
    }

    friend RealNumber cos(const RealNumber& hs){
        return RealNumber(AD_COS, cos(hs.value), hs);
    }

    friend RealNumber sin(const RealNumber& hs){
        return RealNumber(AD_SIN, sin(hs.value), hs);
    }

    friend RealNumber tan(const RealNumber& hs){
        return RealNumber(AD_TAN, tan(hs.value), hs);
    }

    friend RealNumber acos(const RealNumber& hs){
        return RealNumber(AD_ACOS, acos(hs.value), hs);
    }

    friend RealNumber asin(const RealNumber& hs){
        return RealNumber(AD_ASIN, asin(hs.value), hs);
    }

    friend RealNumber atan(const RealNumber& hs){
        return RealNumber(AD_ATAN, atan(hs.value), hs);
    }

    friend RealNumber atan2(const RealNumber& lhs, const RealNumber& rhs){
        RealNumber y = lhs / rhs;
        return RealNumber(AD_ATAN, atan(y.value), y);
    }

    friend RealNumber fabs(const RealNumber& hs){
        if (hs.value >= 0) {
            return hs;
        } else {
            return RealNumber(AD_NEG, -hs.value, hs);
        }
    }

    friend RealNumber fmod(const RealNumber& lhs, const RealNumber& rhs){
        // TODO: implement.
        BOOST_ASSERT_MSG(false, "fmod is not implemented yet");
        return RealNumber(AD_NEG, -lhs.value, lhs);
    }

    friend bool operator==(const RealNumber& lhs, const RealNumber& rhs){
        return lhs.value == rhs.value;
        //TODO one might expect true for sufficiently close two floating points
        //TODO what if one wants to compare including d_var??
    }

    friend bool operator!=(const RealNumber& lhs, const RealNumber& rhs){
        return lhs.value != rhs.value;
    }

    friend bool operator>(const RealNumber& lhs, const RealNumber& rhs){
        return lhs.value > rhs.value;
    }

    friend bool operator<(const RealNumber& lhs, const RealNumber& rhs){
        return lhs.value < rhs.value;
    }

    friend bool operator>=(const RealNumber& lhs, const RealNumber& rhs){
        return lhs.value >= rhs.value;
    }

    friend bool operator<=(const RealNumber& lhs, const RealNumber& rhs){
        return lhs.value <= rhs.value;
    }

    void attach_to_tape(Tape<T>& tape){ //TODO is that the RealNumber's responsibility to attach to a tape? or the tape's?
        attached_tape = &tape;
        attached_location_on_tape = tape.end_pointer;
        new (tape.end_pointer++) History<T> (AD_NONE, value, 0, nullptr,
                nullptr);
    }

    T get_diff() const{
        return attached_location_on_tape->diff;
    }
    void diff() const{
        differentiate(this->attached_tape->memory_block);
    }
    void differentiate(History<T>* until_this_pointer) const{
        attached_location_on_tape->diff = 1;
        for (History<T>* i = attached_location_on_tape - 1; i != until_this_pointer
                - 1; --i){
            i->diff = 0;
        }
        for (History<T>* i = attached_location_on_tape; i != until_this_pointer
                - 1; --i){
            switch (i->operation_type) {
            case AD_NONE:
                break;
            case AD_ADD:
                if (i->op1 != nullptr)
                    i->op1->diff += i->diff;
                if (i->op2 != nullptr)
                    i->op2->diff += i->diff;
                break;
            case AD_SUB:
                if (i->op1 != nullptr)
                    i->op1->diff += i->diff;
                if (i->op2 != nullptr)
                    i->op2->diff -= i->diff;
                break;
            case AD_NEG:
                i->op1->diff += -(i->diff);
                break;
            case AD_MUL:
                //                if (i->op1 != nullptr) // TODO when i->op1->value == 0
                //                    i->op1->diff += i->diff * i->value / i->op1->value;
                //                if (i->op2 != nullptr)
                //                    i->op2->diff += i->diff * i->value / i->op2->value;
                i->op1->diff += i->diff * i->op2->value;
                i->op2->diff += i->diff * i->op1->value;
                break;
            case AD_MUL1:
                i->op1->diff += i->diff * i->op2_T;
                break;
            case AD_MUL2:
                i->op2->diff += i->diff * i->op1_T;
                break;
            case AD_MUL_CONSTANT:
                BOOST_ASSERT_MSG(false,
                        "YOU SHOULDN'T REACH HERE SINCE THIS IS CONSTANT * CONSTANT");
                break;
            case AD_EXP:
                i->op1->diff += i->diff * i->value;
                break;
            case AD_LOG:
                i->op1->diff += i->diff / i->op1->value;
                break;
            case AD_DIV:
                i->op1->diff += i->diff / i->op2->value;
                i->op2->diff += i->diff * (-i->op1->value) / (i->op2->value
                        * i->op2->value);
                break;
            case AD_POW1:
                //c = a^b
                //dc/da = b*a^(b-1)
                //log c = b log a
                // b = log_a{c}
                //                tmp1 = log(i->value) / log(i->op1->value);
                //                i->op1->diff += i->diff * (tmp1 * pow(i->op1->value, tmp1 - 1));
                if (i->op1->value == 0){
                    //do nothing
                }else{
                    i->op1->diff += i->diff * (i->op2_T * i->value
                            / i->op1->value);
                }
                break;
            case AD_POW:
                // TODO special case when op1->value is 0?    
                i->op1->diff += i->diff * (i->op2->value * i->value
                        / i->op1->value);
                i->op2->diff += i->diff * i->value * log(i->op1->value);
                break;
            case AD_SQRT:
                i->op1->diff += i->diff * 1 / (2 * sqrt(i->op1->value));
                break;
            case AD_COS:
                i->op1->diff += i->diff * (-sin(i->op1->value));
                break;
            case AD_SIN:
                i->op1->diff += i->diff * cos(i->op1->value);
                break;
            case AD_TAN:
                i->op1->diff += i->diff * 1 / (i->op1->value*i->op1->value + 1);
                break;
            case AD_ACOS:
                i->op1->diff += i->diff * (-1 / sqrt(1 - i->op1->value * i->op1->value));
                break;
            case AD_ASIN:
                i->op1->diff += i->diff * (1 / sqrt(1 - i->op1->value * i->op1->value));
                break;
            default:
                BOOST_ASSERT_MSG(false, "NOT IMPLEMENTED");
                break;
            }
        }
    }
};
}

namespace std {
template<typename T> //Todo we can probably use template alias feature in c++11
struct numeric_limits<kklautodiff::RealNumber<T>> : public numeric_limits<T> {
};
}

#endif
