#include <iostream>

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "../kklautodiff.hpp"

using namespace std;

BOOST_AUTO_TEST_CASE(test1)
{
    kklautodiff::RealNumber<double> a = 3;
    kklautodiff::RealNumber<double> b = 5;
    kklautodiff::Tape<double> tape(1000);
    a.attach_to_tape(tape);
    b.attach_to_tape(tape);
    kklautodiff::RealNumber<double> c = a * b;
    c.diff();
    BOOST_CHECK_CLOSE(c.value, 15, 0.0000001);
    BOOST_CHECK_CLOSE(a.get_diff(), 5, 0.0000001);
    BOOST_CHECK_CLOSE(b.get_diff(), 3, 0.0000001);
}

BOOST_AUTO_TEST_CASE(test2)
{
    kklautodiff::RealNumber<double> a = 10;
    kklautodiff::RealNumber<double> b = 5;
    kklautodiff::Tape<double> tape(1000);
    a.attach_to_tape(tape);
    b.attach_to_tape(tape);
    kklautodiff::RealNumber<double> c = a / b;
    c.diff();
    BOOST_CHECK_CLOSE(c.value, 2, 0.0000001);
    BOOST_CHECK_CLOSE(a.get_diff(), 0.2, 0.0000001);
    BOOST_CHECK_CLOSE(b.get_diff(), -10.0/25, 0.0000001);
}

BOOST_AUTO_TEST_CASE(test3)
{
    kklautodiff::RealNumber<double> a = 10;
    kklautodiff::Tape<double> tape(1000);
    a.attach_to_tape(tape);
    kklautodiff::RealNumber<double> c = a / 5;
    c.diff();
    BOOST_CHECK_CLOSE(c.value, 2, 0.0000001);
    BOOST_CHECK_CLOSE(a.get_diff(), 0.2, 0.0000001);
}

BOOST_AUTO_TEST_CASE(test4)
{
    kklautodiff::RealNumber<double> b = 5;
    kklautodiff::Tape<double> tape(1000);
    b.attach_to_tape(tape);
    kklautodiff::RealNumber<double> c = 10 / b;
    c.diff();
    BOOST_CHECK_CLOSE(c.value, 2, 0.0000001);
    BOOST_CHECK_CLOSE(b.get_diff(), -10.0/25, 0.0000001);
}

BOOST_AUTO_TEST_CASE(test5)
{
    kklautodiff::RealNumber<double> a = 4;
    kklautodiff::Tape<double> tape(1000);
    a.attach_to_tape(tape);
    kklautodiff::RealNumber<double> c = sqrt(a);
    c.diff();
    BOOST_CHECK_CLOSE(c.value, 2, 0.0000001);
    BOOST_CHECK_CLOSE(a.get_diff(), 0.25, 0.0000001);
}

BOOST_AUTO_TEST_CASE(test6)
{
    kklautodiff::RealNumber<double> a = 0;
    kklautodiff::Tape<double> tape(1000);
    a.attach_to_tape(tape);
    kklautodiff::RealNumber<double> c = sin(a);
    c.diff();
    BOOST_CHECK_CLOSE(c.value, 0, 0.0000001);
    BOOST_CHECK_CLOSE(a.get_diff(), 1, 0.0000001);
}

BOOST_AUTO_TEST_CASE(test7)
{
    kklautodiff::RealNumber<double> a = 0;
    kklautodiff::Tape<double> tape(1000);
    a.attach_to_tape(tape);
    kklautodiff::RealNumber<double> c = cos(a);
    c.diff();
    BOOST_CHECK_CLOSE(c.value, 1, 0.0000001);
    BOOST_CHECK_CLOSE(a.get_diff(), 0, 0.0000001);
}

BOOST_AUTO_TEST_CASE(test8)
{
    kklautodiff::RealNumber<double> a = 3;
    kklautodiff::Tape<double> tape(1000);
    a.attach_to_tape(tape);
    kklautodiff::RealNumber<double> c = log(a);
    c.diff();
    BOOST_CHECK_CLOSE(c.value, log(3), 0.0000001);
    BOOST_CHECK_CLOSE(a.get_diff(), 1.0/3.0, 0.0000001);
}

BOOST_AUTO_TEST_CASE(test9)
{
    kklautodiff::RealNumber<double> a = 3;
    kklautodiff::Tape<double> tape(1000);
    a.attach_to_tape(tape);
    kklautodiff::RealNumber<double> c = pow(a, 2);
    c.diff();
    BOOST_CHECK_CLOSE(c.value, 9, 0.0000001);
    BOOST_CHECK_CLOSE(a.get_diff(), 6, 0.0000001);
}

BOOST_AUTO_TEST_CASE(test10)
{
    kklautodiff::RealNumber<double> a = -0.0;
    kklautodiff::Tape<double> tape(1000);
    a.attach_to_tape(tape);
    kklautodiff::RealNumber<double> c = pow(a, 1.0/1000);
    c.diff();
    BOOST_CHECK_CLOSE(c.value, 0, 0.0000001);
    BOOST_CHECK_CLOSE(a.get_diff(), 0, 0.0000001);
}

BOOST_AUTO_TEST_CASE(test11)
{
    kklautodiff::RealNumber<double> a = 2.0;
    kklautodiff::RealNumber<double> b = 2.0;
    kklautodiff::Tape<double> tape(1000);
    a.attach_to_tape(tape);
    b.attach_to_tape(tape);

    kklautodiff::RealNumber<double> c = pow(a, b);
    c.diff();
    BOOST_CHECK_CLOSE(c.value, 4, 0.0000001);
    BOOST_CHECK_CLOSE(a.get_diff(), 4, 0.0000001);
    BOOST_CHECK_CLOSE(b.get_diff(), 22.77258872223978, 0.0000001);
}